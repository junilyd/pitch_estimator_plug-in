/**
 * Reads a wav file into buffer.
 * @param *inputFileName string with input file path and name.
 * returns a float* pointer to input buffer
 */
float* smc_wavread(char *inputFileName, char* outputFileName);
/**
 * Outputs a txt file with given input data
 * @param *outputBuffer Pointer to input signal
 * @param *name string containing output file path and name
 * @param N Length of output signal (<= input buffer)
 */
void smc_write_file_float(float inputSignal[], char *name, int N);
/**
 * Outputs a txt file with given input data
 * @param *columnOne pointer to float input vector 1
 * @param *columnTwo pointer to float input vector 2
 * @param *name string containing output file path and name
 * @param N Length of output signal (<= input buffer)
 */
void smc_write_file_float_2columns(float* columnOne, float* columnTwo, char* name, int N)
;