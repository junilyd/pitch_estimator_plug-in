/*
 * This program gives pitch estimate from an audio file input. 
 * It is estimating pitch by ANLS.
 * TODO: improve by implementing smoother and order selection
 *       and finer resolution on estimates.
 *       Implement in real time plugin
 *       Move allocation from inside fft function
 *       should be done globally instead of locally.
 */
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include "smc_anls.h"
#include "smc_fileio.h"
#include <fftw3.h>

char inputFileName[] = "data/5.wav"; //../../../../../Dropbox/testfiles/frets/string3/10.wav";
char outputFileName[] = "data/input_signal.txt";
char FFTOutputFileName[] = "data/fft.txt";
char costOutputFileName[] = "data/cost.txt";

float F0Res=0.1;
float F0Max=340.1, F0Min=70.1;
int L=4, Fs=44100;

int NFFT = 44100*2;

int main()
{
    int F0AreaSize = round((F0Max-F0Min)/F0Res+1);
    // Memory Allocation
	float *F0Area;
    F0Area = (float *)malloc(F0AreaSize*sizeof(float));
	float *cost;
    cost = (float *)malloc(F0AreaSize*sizeof(float));
    float *X;
    X = (float *)malloc(NFFT*sizeof(float));

    // Read wav-file
    float* sig;
    sig = smc_wavread(inputFileName, outputFileName);

    // Make FFT
//	int NFFT = Fs; 
    X = smc_fft(sig, NFFT, Fs, FFTOutputFileName);
    
    // Create Cost
    smc_generate_F0Area(F0Min, F0Max, F0Res, F0Area, F0AreaSize);
    smc_create_HS_cost(Fs, L, X, F0Area, cost, round(NFFT/2), F0AreaSize);

    // Estimate Pitch by Maximizing Cost
    int argMax;
    argMax = smc_arg_max(cost, F0AreaSize);
    float pitchEstimate;
    pitchEstimate = F0Area[argMax];
    // Pitch Estimate
    printf("\n pitchEstimate is: %f Hz\n", pitchEstimate);


    // output cost for plotting
    smc_write_file_float_2columns(F0Area, cost, costOutputFileName, F0AreaSize);

// Cleanup
free(F0Area);
free(X);
free(cost);
return 0;
}