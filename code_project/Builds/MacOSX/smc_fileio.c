#include <stdio.h>
#include <stdlib.h>
    

void smc_write_file_float(float inputSignal[], char *name, int N)
{
    FILE *outFile;
    int n;

    outFile=fopen(name, "w");
    if (outFile) {
        for(n=0; n < N; n++)
        fprintf(outFile, "%f\n", inputSignal[n]);
        fclose(outFile);
    }
}

void smc_write_file_float_2columns(float* columnOne, float* columnTwo, char* name, int N)
{
    FILE *outFile;
    int n;

    outFile=fopen(name, "w");
    if (outFile) {
        for(n=0; n < N; n++)
        fprintf(outFile, "%f \t %f \n", columnOne[n], columnTwo[n]);
        fclose(outFile);
    }
}

