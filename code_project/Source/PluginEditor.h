/*
  ==============================================================================

    This file was auto-generated by the Introjucer!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"


//==============================================================================
/**
*/
class MididemopluginAudioProcessorEditor  : public AudioProcessorEditor,
                                            private Slider::Listener,
                                            private Label::Listener
{
public:
    MididemopluginAudioProcessorEditor (MididemopluginAudioProcessor&);
    ~MididemopluginAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    
    void resized() override;

    void sliderValueChanged (Slider* slider) override;
    
    void labelTextChanged (Label* pitchText) override;
    
private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    MididemopluginAudioProcessor& processor;
    
    Slider sliderVolume;
    Slider sliderHarmonics;
    Label labelPitch;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MididemopluginAudioProcessorEditor)
};


#endif  // PLUGINEDITOR_H_INCLUDED
