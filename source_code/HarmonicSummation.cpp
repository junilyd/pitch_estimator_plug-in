//
//  HarmonicSummation.cpp
//  midi-demo-plugin
//
//  Created by Navn on 19/05/15.
//
//

#include "HarmonicSummation.h"
#include "math.h"
#include <stdlib.h>

// constructor
HarmonicSummation::HarmonicSummation () {
}

// destructor
HarmonicSummation::~HarmonicSummation () {
}

void HarmonicSummation::generateCost(float* X, float* Cost, float* f0Area, int L, int bufsize, int f0AreaSize, int fs){
    int fIndex;     // One sample of F0Area
    for (int n = 0; n < f0AreaSize; n++)
    {
        fIndex = (int)round(f0Area[n]*(4.f*bufsize/fs)+1);
        Cost[n] = X[fIndex];
        // Harmonic Summation
        for (int l = 2; l <= L; l++)
        {
            fIndex = (int)round(f0Area[n]*l*(4.f*bufsize/fs)+1);
            Cost[n] += X[fIndex];
        }
    }
}

int HarmonicSummation::argMax (int f0AreaSize, float* Cost) {
    int argument;
    float maxVal = -32000;
    
    for (int i = 0; i < f0AreaSize; i++)
    {
        if (maxVal < Cost[i]) {
            maxVal = Cost[i];
            argument = i;
        }
    }
    return argument;
}