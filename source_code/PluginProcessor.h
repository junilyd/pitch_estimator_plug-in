
#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "FastFourierTransformer.h"
#include "HarmonicSummation.h"

//==============================================================================
/**
*/
class MididemopluginAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    MididemopluginAudioProcessor();
    ~MididemopluginAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    int getNumParameters() override;
    float getParameter (int index) override;
    void setParameter (int index, float newValue) override;

    const String getParameterName (int index) override;
    const String getParameterText (int index) override;

    const String getInputChannelName (int channelIndex) const override;
    const String getOutputChannelName (int channelIndex) const override;
    bool isInputChannelStereoPair (int index) const override;
    bool isOutputChannelStereoPair (int index) const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool silenceInProducesSilenceOut() const override;
    double getTailLengthSeconds() const override;

    //=============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    
    // Controller that we have added
    float noteOnVel;
    
    // For Fourier Transform Class
    float gain, oldGain;
    FastFourierTransformer* fft;
    fftw_complex*  fftData;
    
    // For Harmonic Summation
    HarmonicSummation* HS;
    float f0Min;
    float f0Max;
    float f0Res;
    float* f0Area;
    float* cost;
    float* X;
    int f0AreaSize, numberOfHarmonics;
    float pitchEstimate;
    String pitchText;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MididemopluginAudioProcessor)
};


#endif  // PLUGINPROCESSOR_H_INCLUDED
