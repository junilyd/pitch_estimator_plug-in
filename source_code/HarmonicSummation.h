//
//  HarmonicSummation.h
//  midi-demo-plugin
//
//  Created by Jacob Møller on 19/05/15.
//
//

#ifndef __midi_demo_plugin__HarmonicSummation__
#define __midi_demo_plugin__HarmonicSummation__

#include <stdio.h>

#endif /* defined(__midi_demo_plugin__HarmonicSummation__) */

class HarmonicSummation {
    
public:
    
    HarmonicSummation ();
    ~HarmonicSummation();
    
    // Find Arg Max
    int argMax              (int f0AreaSize, float* Cost);
    void generateCost       (float* X, float* Cost, float* f0Area, int L, int bufsize, int f0AreaSize, int fs);

private:
    
    float *HS;
};