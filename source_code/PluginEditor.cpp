/*
  ==============================================================================

    This file was auto-generated by the Introjucer!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
MididemopluginAudioProcessorEditor::MididemopluginAudioProcessorEditor (MididemopluginAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);
    
    // these define the parameters of our slider object
    sliderVolume.setSliderStyle (Slider::LinearBarVertical);
    sliderVolume.setRange(0.0, 127.0, 1.0);
    sliderVolume.setTextBoxStyle (Slider::TextBoxAbove, false, 90, 0);
    sliderVolume.setPopupDisplayEnabled (true, this);
    sliderVolume.setTextValueSuffix (" MIDI_Volume");
    sliderVolume.setValue(80.0);
    
    // these define the parameters of our gain object
    sliderHarmonics.setSliderStyle (Slider::LinearBarVertical);
    sliderHarmonics.setRange(1, 15, 1);
    sliderHarmonics.setTextBoxStyle (Slider::TextBoxAbove, true, 90, 0);
    sliderHarmonics.setPopupDisplayEnabled (true, this);
    sliderHarmonics.setTextValueSuffix ("Harmonic Order");
    sliderHarmonics.setValue(5);
    
    labelPitch.setBounds(120, 120, 200, 200);
    labelPitch.setText(processor.pitchText, sendNotification);
    labelPitch.setFont(60.0f);

    // Add the listener to the slider
    sliderVolume.addListener(this);
    sliderHarmonics.addListener(this);
    
    //Add Listener to the pitchestimate label
    labelPitch.addListener(this);
    
    // this function adds the slider to the editor
    addAndMakeVisible (&sliderVolume);
    addAndMakeVisible (&sliderHarmonics);
    
    addAndMakeVisible(&labelPitch);
}

MididemopluginAudioProcessorEditor::~MididemopluginAudioProcessorEditor()
{
}

//==============================================================================
void MididemopluginAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (Colours::white);
    g.setColour (Colours::black);
    g.setFont (15.0f);
    g.drawFittedText ("Pitch Box", 0, 0, getWidth(), 30, Justification::centred, 1);
    g.drawFittedText("Hz", 60, 220, getWidth(), 30, Justification::centred, 1);
}

void MididemopluginAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    // sets the position and size of the slider with arguments (x, y, width, height)
    sliderVolume.setBounds (40, 30, 20, getHeight() - 60);
    sliderHarmonics.setBounds (360, 30, 20, getHeight() - 60);

}
// Function to change value for sliderVolume slider
void MididemopluginAudioProcessorEditor::sliderValueChanged (Slider* slider)
{
    processor.noteOnVel = sliderVolume.getValue();
    processor.numberOfHarmonics = sliderHarmonics.getValue();
}

void MididemopluginAudioProcessorEditor::labelTextChanged (Label* labelThatHasChanged)
{
    labelPitch.setText(processor.pitchText, sendNotification);
}
