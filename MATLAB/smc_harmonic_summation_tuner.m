%   ----------------------------------------------------------
%   Estimates the pitch from harmonic summation
%   Here the tuner also zooms in to get some calc. efficiency
%   --------------------------------------------------------------------------------
%   INPUTS:
%           X:       fft of input signal
%           f0_area: Vector of fundamental frequrncies in hertz (the search area)
%           L:       Number of Harmonics to use.
%           fs:      Sample rate to use (usually samplerate of input signal)
%   OUTPUTS:
%           pitch:   The pitch estimate
% --------------------------------------------------------
% pitch3 = smc_harmonic_summation_tuner(X, f0_area, L, fs)
% --------------------------------------------------------
% version 1.0 jmhh
% version 1.1 anch : if used in EM, high resolution f0_area as input
% lower resolution and faster computation
% 2nd search area has changed to +-0.5 from +-4 Hz.
function pitch3 = smc_harmonic_summation_tuner2(X, f0_area, L, fs,EM_f0Area)
if nargin < 5
f0_area = [min(f0_area):0.1:max(f0_area)];
end


i=1;
for f=f0_area
    [index] = smc_harmonic_index(X, fs, L, f);
    cost(i) = sum(X(index)); i = i+1;
end
[C, I] = smc_max(cost);
pitch = f0_area(I); 

f0_area2 = [pitch-4:0.01:pitch+4]; % changed from +-4 Hz
i=1;
for f=f0_area2
    [index] = smc_harmonic_index(X, fs, L, f);
    cost2(i) = sum(X(index));i=i+1;
end
[C,I] = smc_max(cost2);
pitch2 = f0_area2(I);

f0_area3 = [pitch2-0.01:0.001:pitch2+0.01];
i=1;
for f=f0_area3
    [index] = smc_harmonic_index(X, fs, L, f);
    cost3(i) = sum(X(index));i=i+1;
end
[C,I] = smc_max(cost3);
pitch3 = f0_area3(I);

% % extra under test
% f0_area4 = [pitch3-0.001:0.0001:pitch3+0.001];
% i=1;
% for f=f0_area4
%     [index] = smc_harmonic_index(X, fs, L, f);
%     cost4(i) = sum(X(index));i=i+1;
% end
% [C,I] = smc_max(cost4);
% pitch4 = f0_area4(I);
end
