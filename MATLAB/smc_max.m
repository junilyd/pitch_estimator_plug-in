% ----------------------------------------------
% locates the maximum value of the input signal.
% 
% INPUT:  
%       sig     : input signal 
% OUTPUT: 
%       max     : maximum value
%       argmax  : index of the maximum value.
%
% -----------------------------
% [max_, argmax] = smc_max(sig-)
% -----------------------------
%
function [max_, argmax] = smc_max(sig)
    max_ = -inf;
    argmax=0;
    for i=1:length(sig)
        if  max_ < sig(i)
            max_ = sig(i);
            argmax= i;
        end
    end
end

