% ----------------------------------------------------------------------
% calcualates the FFT of the input signal. returns up to nyquist freq.
% It does the zeropadding up to next power of 2 with the built a MATLAB function.
%
% INPUT: 
%       sig: signal to analyse
%       Fs:  sample rate of that signal
% OUTPUT:
%       f_axis:     x-axis for plotting
%       fft_sig:    linear fft signal
%       fft_sig_dB: logarithmic fft signal in dB
%       NFFT:       the power of 2 used for the fft
%      
% EXAMPLE: 
%         For using this with an audio file:    [sig,fs] = audioread('testfiles/A_mid.wav');
%                                               [f,fft_sig, fft_dB] = smc_fft(sig,fs);
%                                               plot(f,fft_sig); figure;
%                                               plot(f,fft_dB);
%         
% -----------------------------------------------------------
% function [f_axis, fft_sig, fft_sig_dB] = smc_fft(sig,Fs)    
% -----------------------------------------------------------
function [f_axis,fft_sig, fft_sig_dB] = smc_fft(sig,Fs)    
    N = length(sig);
    NFFT     = 2^nextpow2(N);
    fft_long = fft(sig,NFFT);
    fft_sig  = 2*abs(fft_long(1:NFFT/2)).^2;
    fft_sig_dB = 10*log10(fft_sig);
    f_axis   = Fs/2 * linspace(0,1,NFFT/2);
end
